
# Customizing vim 

Like most command line programs, vim can be customized using an _rc-file_
available from your home directory, try `cat ~/.vimrc`.
An example vimrc can be found here `~/vim/runtime/vimrc_example.vim` assuming
vim was downloaded to this directory.

After introducing some minimal information on _vimscript_, the language used
by vim, we will go over some useful customization of native vim 
without the need for third-party plugins.

## vimscript

Vimscript is the native scripting language for vim, and similar to 
bash or python, in the sense that it can collect data in lists and arrays. 
As such, for most people -- even with limited coding exposure vimscript
will not be a major obstacle. 

Nevertheless, a few things to note:
1. Comments are preceded by a "
2. Lines can be split by placing a '\\', on the _following_ line; contrary to
python where '\\' is used to end, or break, the line

Further details on vimscript can be readily found online,
for example [here](https://devhints.io/vimscript), or through the 
help function `:h vimscript<CR>`.

## Options that can only be set once

Vim is customizable through `.vimrc` and various other files in the `~/.vim` 
directory, often loaded after `.vimrc`.
As such some options may be inadvertently overwritten. 
To prevent this happening to essential arguments, some mappings can only be 
set once.

One of the more useful mappings is the `<leader>`, which is mapped to `\` key, 
and often hard to reach.
You will find that the  `<leader>` key is essential as a prefix to many 
useful keyboard combinations.  For example to turn of search highlights,
run `<leader><space>`.

Let's remap the `<leader>` to `,` ,and for clarities sake includes this 
at the top of our `.vimrc`; run `vim ~/.vimrc`.

```{vim}
" map leader to comma
let mapleader = ','
" map reverse search to \
noremap \ ,
```

Now write `:w` and source the current buffer `source %`.

As an example of the `<leader>` key functionality we will consider the 
following.
I very much appreciate the Emacs bindings to position the cursor at 
the start- or end of a line.
However, vim uses these bindings to add `C-a`
or subtract `C-x` a number.
To free up these mappings we will remap them: 

```{vim}
" remapping the addition and subtraction commands
nnoremap <leader>a <C-a>
nnoremap <leader>x <C-x>
```

Now you can increment, or decrement, a number in normal mode by positioning 
the cursor anywhere on the line in front of that number, and rattle off the 
combination `,a` to add by 1, or `2,x` to subtract by 2.

## Types of vim mappings

We have re-mapped commands using the `map` and the `nnoremap` commands.
You might be wondering what the difference is? 

Try `:h map<CR>`, you should find that `map` creates a key
combination that works in _normal_, _visual_, _select_ and _operation pending_ modes. Similarly `map!` would create a mapping to _insert_ and 
_command line_ modes.

`nnoremap` is a bit more specific, and the mappings will only apply to 
normal mode.
This is very similar to the `nmap` modes -- which has perhaps 
a clearer mnemonic.
The difference between the to `nnoremap` and `nmap`, is that the 
former prevents any _recursive_ mapping. For example try 

```{vim}
:nmap - dd<CR>
:nmap \ -<CR>
```
Now try `\` in normal modes, what happens? 

Mappings can be specified for any kind of mode:

```{vim}
:nmap - Display normal mode maps
:imap - Display insert mode maps
:vmap - Display visual and select mode maps
:smap - Display select mode maps
:xmap - Display visual mode maps
:cmap - Display command-line mode maps
:omap - Display operator pending mode maps
```

Further readings:

1. https://vim.fandom.com/wiki/Mapping_keys_in_Vim_-_Tutorial_(Part_1)
2. https://learnvimscriptthehardway.stevelosh.com/chapters/05.html

## Creating some further shortcuts

Vim is a modular editor, meaning that depending on the current mode 
key bindings will mean different things. 

One of the main modes you will want to switch between is _normal_, 
where you move around, and _insert_ mode, where you add or remove
text. 
By default you enter insert mode by typing `i` in normal mode, 
and return to normal mode by pressing `<ESC>`. 
Escape is difficult to reach, so we might want to remap this.
I have also listed the Emacs bindings mentioned before, and 
a mappings to delete from insert mode:

```{vim}
"------------------------------------
" Remapping default bindings
"------------------------------------
inoremap ii <esc>

"Move to the end of a line EMACS
inoremap <C-e> <C-o>$
nnoremap <C-e> $

"Move to the start of a line EMACS
inoremap <C-a> <C-o>0
nnoremap <C-a> 0

" Delete from insert mode
inoremap <C-l> <Del>
```

Note, that `C` should be interpreted as `Ctrl`.

Once you start to explore vim, you will be tempted to add a newline 
from normal mode using `<CR>`.
To get the desired effect, with the distinction of before or after the 
current positions, add the following to your `.vimrc`:

```{vim}
" Normal mode: enter starts a new line
nnoremap <leader><CR> O<Esc>
nnoremap <CR> o<Esc>
```

Similarly, the following bindings allows you to move lines up, or down:

```{vim}
"------------------------------------
" MOVING LINES
"------------------------------------
nnoremap <c-j> :m .+1<CR>==
nnoremap <c-k> :m .-2<CR>==
inoremap <c-j> <Esc>:m .+1<CR>==gi
inoremap <c-k> <Esc>:m .-2<CR>==gi
vnoremap <c-j> :m '>+1<CR>gv=gv
vnoremap <c-k> :m '<-2<CR>gv=gv
```

## Indentation

Most coding languages allow for some kind of indentation, 
often clearly communicates nesting.
For languages such a python indentations
are also recognized by the interpreter.
The following is my 
default settings, please explore using the help `ex` command, 
e.g., `h nosmartindent`.


```{vim}
"------------------------------------
" Spaces & Tabs
"------------------------------------
set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces
set shiftwidth=4    " indentation tabs
set autoindent
set nosmartindent
```

## Additional interface configurations

Currently I am using the following UI options:

```{vim}
"------------------------------------
" UI Config
"------------------------------------
set showcmd             " show command in bottom bar
" set cursorline          " highlight current line; SLOW
set wildmenu            " visual autocomplete for command menu
set lazyredraw          " redraw only when we need to.
set showmatch           " highlight matching [{()}]
set number norelativenumber   " do not show relative line numbers
set splitbelow          " split windows to the bottom.
set winheight=30        " determines the terminal height
set clipboard=unnamed   " allows yank and put between terminals
```

The `set showcmd` is actually turned on by default already and simply 
print the commands used in normal and other modes in the lower part of 
the screen -- the statusline.

`wildmenu`, is useful to tab-completion. See 
[here](https://vim.fandom.com/wiki/Line/word/file/whatever_completion),
and further down.

`lazyredraw`, to speed up the terminal.

`showmatch`, to highlight matching brackets.

The remaining should be fairly self-explanatory. 

Personally, I prefer to have the standard line numbers: starting at the top of
a file all the way down. 
As such I include the `set number norelativenumber` option in my `.vimrc`. 
Nevertheless, 
I do appreciate relative line numbers when in _visual_ mode. 
The problem is that this option does not exist in vim (yet?), instead I will 
leave you with a vim function that accomplishes and can be included in your
`.vimrc`.

```{vim}
"----------- auto-toggle line numbers
function! s:ModeCheck(id)
    let vmode = mode() =~# "[vV\<C-v>]"
    if vmode && !&rnu
    set relativenumber
    elseif !vmode && &rnu
    set norelativenumber
    endif
endfunction
call timer_start(100, function('s:ModeCheck'), {'repeat': -1})
```

## Custom colours and highlights 

To allow vim to colour or highlight code, or matching text snippets, one has
to turn on _syntax highlighting_ by adding the following to your .vimrc

```{vim}
"------------------------------------
" Syntax highlight and colours
"------------------------------------
syntax enable           
```

Depending on the colour scheme, which can be installed as a [plugin](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/plugins.md), 
we might need to inform vim on the number of terminal colours it can use. 
For example we could add the following:

```{vim}
"------------------------------------
" Colour scheme
"------------------------------------
set termguicolors
```

## Searching 

I have found the following options useful to search through a vim buffer, 
using `/` or `?`; for forwards or backwards.

```{vim}
"------------------------------------
" Searching
"------------------------------------
set incsearch                   " search as characters are entered
set hlsearch                    " highlight matches
set wildmode=list:longest,full  " tabcompletion mode, with tab cycle
set ignorecase                  " ignores cases when searching
set smartcase                   " see :h smartcase<CR>
```

You will notice that matching text will be highlighted. 
The next mapping tells vim to stop showing this. 

```{vim}
" Mapping 'turn off search highlight' to <space>
nnoremap <leader><space> :nohlsearch<CR>
```

## Moving through buffers

Vim can load multiple files, or buffers. 
There are many ways to move through all of them.
The following mappings allows you to cycle through them using `[b` or `]b`.

```{vim}
"remapping buffer list commands
nnoremap <silent> [b :bprevious<CR>
nnoremap <silent> ]b :bnext<CR>
nnoremap <silent> [B :bfirst<CR>
nnoremap <silent> ]B :blast<CR>
```

## Automatically run vim code

### Run code on save

Add the following to your `.vimrc` to set all shebang files to 
executable without leaving vim.

```{vim}
"------------------------------------
" If shebang, set to be exacutable
"------------------------------------
au BufWritePost * if getline(1) =~ "^#!" | if getline(1) =~ "/bin/" | silent execute "!chmod +x <afile>" | endif | endif
```

Here the `BufWritePost` is used to tell vim to run the following commands 
when the buffer is written (that is saved).

### Run code for specific files only

Similarly, you can tell vim to only load specific `.vimrc` code 
if a certain file type is recognized. 
For example, the following ensures vim appropriately recognizes `.json`
syntax as comments.

```{vim}
"------------------------------------
" JSON
"------------------------------------
# makes vim recknoze json comments
autocmd FileType json syntax match Comment +\/\/.\+$+
```

## Completion

Vim can complete your text and code from various sources. 
For example, to insert a file name or path `<C-x><C-f>`,
or completion from the current buffer using `<C-x><C-n>`.

Similarly, vim can also complete words from a dictionary
using `<C-x><C-k>`. 
We do need to tell vim where to find one, or multiple, dictionary files.
For example,

```{vim}
"------------------------------------
" Add dictionary locations
"------------------------------------
set complete+=k " adding dictionary to completion
set dictionary+=/usr/share/dict/words
set dictionary+=/usr/share/dict/british-english
set dictionary+=/usr/share/dict/american-english
```

While looking through the help files for this section, it seems
vim also comes with a _thesaurus_ pre-installed, see `h: thesaurus<CR>`.

