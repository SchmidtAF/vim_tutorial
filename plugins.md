# Plugins 

Native vim is very customizable, and it actually filled to be brim with many
features that will make your work easier. 

See for example vim's integration with `Universal ctags` [here](https://vim.fandom.com/wiki/Browsing_programs_with_tags) and [here](https://github.com/universal-ctags/ctags). 

When first learning things, it might be better to get used to native 
vim, and be very selective with plugins. 
Preferably use plugins that are language agnostic, and that **add** 
functionality instead of _change_ or even _break_ native vim. 

After explaining this, let us go over some plugins that may nevertheless be 
(very) useful.

## Native vim plugins

Vim also includes some plugins that are turned off by default. 
If you have decided to use the example `.vimrc`, it will include a line 
like this

```vim
packadd! matchit
```

The `matchit` plugin extend the behaviour of `%` which cycles between matching
brackets. 
Exert from the help file `:h matchit`:

```vim
1. Extended matching with "%"				*matchit-intro*

%	Cycle forward through matching groups, such as "if", "else", "endif",
	as specified by |b:match_words|.

g%	Cycle backwards through matching groups, as specified by
	|b:match_words|.  For example, go from "if" to "endif" to "else".

[%	Go to [count] previous unmatched group, as specified by
	|b:match_words|.  Similar to |[{|.

]%	Go to [count] next unmatched group, as specified by
	|b:match_words|.  Similar to |]}|.

a%	In Visual mode, select the matching group, as specified by
	|b:match_words|, containing the cursor.  Similar to |v_a[|.
	A [count] is ignored, and only the first character of the closing
	pattern is selected.

In Vim, as in plain vi, the percent key, |%|, jumps the cursor from a brace,
bracket, or paren to its match.  This can be configured with the 'matchpairs'
option.  The matchit plugin extends this in several ways:

	    You can match whole words, such as "if" and "endif", not just
	single characters.  You can also specify a |regular-expression|.
	    You can define groups with more than two words, such as "if",
	"else", "endif".  Banging on the "%" key will cycle from the "if" to
	the first "else", the next "else", ..., the closing "endif", and back
	to the opening "if".  Nested structures are skipped.  Using |g%| goes
	in the reverse direction.
	    By default, words inside comments and strings are ignored, unless
	the cursor is inside a comment or string when you type "%".  If the
	only thing you want to do is modify the behavior of "%" so that it
	behaves this way, you do not have to define |b:match_words|, since the
	script uses the 'matchpairs' option as well as this variable.
```

To get the best of `matchit` you need to tell vim to allow filetype specific
configurations by adding:

```vim
"------------------------------------
""" -------------- Filetype specific settings
"------------------------------------
filetype plugin on
```

## Plugin manager

Recent versions of vim come with a native [plugin manager](https://medium.com/@paulodiovani/installing-vim-8-plugins-with-the-native-pack-system-39b71c351fea), please go ahead and try it. 

Alternatively, you can install one of many plugin managers, for example 
`minpac`:

```vim
# create minpac directory 
mkdir -p $VIMCONFIG/pack/minpac/opt
# move to the directory 
cd !$
# clone minpac
git clone https://github.com/k-takata/minpac.git
# next open your .vimrc
vim ~/.vimrc
# add the following lines to your .vimrc
packadd minpac
call minpac#init()
" minpac command
command! PackUpdate call minpac#update()
command! PackClean call minpac#clean()
# write and source your .vimrc
:write
:source %
```

Now we can install new packages by running `:PackUpdate`, and remove packages
by `:PackClean`

## Colour schemes

Next we will install some colour schemes to better visualize your code. 
Personally I prefer to set a default colour scheme in my `.vimrc` and
add multiple filetype specific colour schemes; file to specific settings 
are discussed [here](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/vim_directory_structure.md).

Some colours:

```vim
"------------------------------------
" colour schemes
"------------------------------------
call minpac#add('reedes/vim-colors-pencil')     " good for writing markdown
call minpac#add('morhetz/gruvbox')
call minpac#add('cocopon/iceberg.vim')
call minpac#add('arcticicestudio/nord-vim')
call minpac#add('fneu/breezy')
```

Remember to save your changes `:w`, reload your _.vimrc_ `so %`, and install 
your packages `PackUpdate`. 
Next set a default colour scheme by adding the following to your `.vimrc`:

```vim
" default colorscheme
set termguicolors
colorscheme iceberg
```

## statusline & tabline

To improve the visual presentation, and add some useful information to the
statusline and tabeline, install the following package with configurations:

```{bash}
"------------------------------------
" Airline
"------------------------------------
call minpac#add('vim-airline/vim-airline')
call minpac#add('vim-airline/vim-airline-themes')

" Remove default modes line
set noshowmode

" colour theme
let g:airline_theme='raven'

" tabs
let g:airline#extensions#tabline#enabled = 1
" let g:airline#extensions#tabline#show_splits = 1
```

## vim register

Text and code can be copy, cut, and pasted using `yw`, `cw`, and `p`;
for example:

```vim
" ADD examples here
```

To see the content of the register, that lists all the available text,
run `:reg`. 

The `peekaboo` plugin allows you to quickly visualize the register content by 
typing `C-r` from the _insert_ mode.

```vim
"------------------------------------
" Calls register
"------------------------------------
call minpac#add('junegunn/vim-peekaboo')
```

## Comments

The following plugin will automatically recognize filtype specific comments
symbols and allows you to toggle lines, paragraphs and the likes using
`gcc`

```vim
"------------------------------------
"Comments
"------------------------------------
call minpac#add('tpope/vim-commentary')
```

## Improved indentation

```vim
"------------------------------------
" indentation guide
"------------------------------------
call minpac#add('nathanaelkane/vim-indent-guides')
let g:indent_guides_enable_on_vim_startup = 1 " show indent by defaul
```

## Add, change, delete brackets and the likes

The following package allows you to add brackets, quotes and similar 
structures around a word using `ysiw"`, delete quotes `ds"`, or
change quotes to brackets `cs"(`.

```vim
"------------------------------------
" surround (for brackets and stuff)
"------------------------------------
call minpac#add('tpope/vim-surround')

" adding vim.repeat to '.' repeat ds, cs and yss
call minpac#add('tpope/vim-repeat')
```

## Minimal ui to write text (markdown, LaTeX, or similar)

Just type `Goyo` to initiate a minimalistic vim mode for writing text instead
of code:

```vim
"------------------------------------
" Goyo
"------------------------------------
call minpac#add('junegunn/goyo.vim') " removes GI for writing

" ---- Goyo settings

" remap goyo
nnoremap <leader>g :Goyo 120x100%<CR>

" what to do when goyo starts
function! s:goyo_enter()
    colorscheme pencil
    " ensure q and q! close all windows
    let b:quitting = 0
    let b:quitting_bang = 0
    autocmd QuitPre <buffer> let b:quitting = 1
    cabbrev <buffer> q! let b:quitting_bang = 1 <bar> q!
endfunction

" closing goyo
function! s:goyo_leave()
    colorscheme distinguished
    " Quit Vim if this is the only remaining buffer
      if b:quitting && len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
        if b:quitting_bang
          qa!
        else
          qa
        endif
      endif
endfunction

" automatically run functions
autocmd! User GoyoEnter nested call <SID>goyo_enter()
autocmd! User GoyoLeave nested call <SID>goyo_leave()
```

## More plugins

After developing your vim skills you may want to try heavier, more advance 
plugins, such as _autocompletion_, code navigation using _ctags_,
filtype specific plugins, and a Read–Eval–Print Loop (REPL) 
plugin to get _R-studio_ or _Jupyter-Notebook_ behaviours. 
I will cover some of these in separate tutorials.

