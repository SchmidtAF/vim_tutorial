# Evaluate code interactively by adding read–eval–print loop (REPL) capability

Most integrated development environments (IDEs) such as Rstudio, pycharm, VSCode
allow the user to send code (chunks) to an interactive terminal - 
often dividing the screen into a top (or invriantly bottom) part were the code 
is written and a bottom part were it can be evaluated. 

## Open a terminal from within vim

For a very long time vim lived by its mantra of not being an IDE, and eschewing
 introducing features that would move it away from its 'simple' text-editor 
origins. 

More recent versions of vim (and nvim from the start) all the user to open a 
terminal running your basic shell using `:terminal<CR>`.

Add the following to your `.vimrc` to have the terminal open a horizontal split
 by default.

```{bash}
set splitbelow              " split windows to the bottom.
set winheight=30            " determines the terminal height
```

To start a different interpreter, python example, run `:term python<CR>`. 

### Basic usage

As always, read vim's extensive help file `:h terminal<CR>`.
To get you started I have included a synopsis of the first view entries.

`CTRL-W` can be used to navigate between windows, for example: 
	`CTRL-W CTRL-W`	move focus to the next window
    `CTRL-W j` move the focus to the bottom window -- for other directions use hjkl
    `CTRL-W J` move the _window_ to the bottom position -- equivalently use HJKL

Use can additionally execute `Ex` commands:
	`CTRL-W CTRL-\`	send a CTRL-\ to the job in the terminal
	`CTRL-W N`	go to Terminal-Normal mode, see |Terminal-mode|
	`CTRL-\ CTRL-N`   go to Terminal-Normal mode, see |Terminal-mode|
	`CTRL-W " {reg}`  paste register {reg}		*t_CTRL-W_quote*
			Also works with the = register to insert the result of
			evaluating an expression.

## REPL plugin

The `CTRL-W` mapping can essentially be used to send any part of the
_code/text_ window to the terminal. 

The following plugin maps much of this functionality to the default
motions. Add the this to your `.vimrc`

```
"------------------------------------
""" --------------REPL plugin
"------------------------------------
call minpac#add('karoliskoncevicius/vim-sendtowindow')
```

Please see [here](https://gitlab.com/SchmidtAF/vim_tutorial/-/blob/master/plugins.md) on how to install and use plugins. 

The plugins' _github_ includes some very good documentation, see [here](https://github.com/karoliskoncevicius/vim-sendtowindow).

Again a short synopsis:
-. To send a _word_ `<w>` to the bottom terminal use `<space>jw` from normal.
-. To send a _line_ to the bottom terminal use `<space>j$` from normal mode.
-. TO send a _paragraph_ to the bottom terminal use `<space>jp` from normal mode.

Clearly the "syntax" is simple: `<space><direction-hjkl>` and you can send line from the _normal_ and _visual_ modes.

Similarly you can send text from the terminal to any other window.
Just be sure to first set the terminal window to normal mode by running 
`CTRL-W N`.


