# Installing or updating vim

In most cases vim will be pre-installed on your machine. 
To check the latest version try:

```bash
vim --version 
# or 
vi --version
```

Likely the installed version will be 7.*, or lower;
warranting an update to version 8 or more recent. 

## 'Updating vim' 

### On MacOS

For MacOS, MacVim can be downloaded [here](https://macvim-dev.github.io/macvim/).
After downloading and moving MacVim to the Applications folder, add the location of executable file to your path, for example like this:
```
export PATH="/Applications/MacVim.app/Contents/bin:$PATH"
```

And then add the following to the alias file, such as `~/.bash_aliases`:
```
alias vim='mvim -v'
```

When typing `vim`, this should now open MacVim.


### On other systems

Instead of directly updating the pre-install version of vim,
we will create a new directory `mkdir ~/vim && cd !$` to 
clone the vim installation files.
Next create a directory where vim will be installed `mkdir ~/usr/vim`.
Now, let's clone and install vim: 

```bash
git clone https://github.com/vim/vim.git
# move to the src directory
cd src
# if this is an actual update clean the previous install
make distclean
# view the possible configurations ./configure --help
# here we will use
./configure --enable-python3interp=dynamic --prefix=$HOME/usr/vim --with-features=huge --with-x
# installing
make
make install
```

#### Configurations

You will notice vim has many installation options available through
`./configure`.
Here we simply chose to install the _huge_ feature set 
(which is actually the default). 
The `--with-x` flag ensures we can copy from vim to other applications. 
Finally we have asked that vim is installed in our home directory 
under `~/usr/vim`.

### Adding vim to your path

Next `vim ~/.bashrc`, alias this version of vim and append it to the front of
your path:

```bash
alias vi='vim'
alias vim='${HOME}/usr/vim/bin/vim'
export PATH="$PATH:{HOME}/usr/vim/bin"
```

Finally `source ~/.bashrc` and confirm the version and compilation date 
`vim --version`.


